Mit diesem Programm lässt sich herausfinden, ob ein eingegebens Passwort in den Datenbanken von haveibeenpwned.com befindet. 
Das Passwort wird dabei nicht an die API von haveibeenpwned übertragen, sondern zuerst gehasht (SHA1) und dann werden die ersten 5 Zeichen des Hashes übertragen.
Die API antwortet mit allen infragekommenden Hashwerten und das Programm prüft, ob sich das eingegebene Passwort in der Rückgabe der API befindet.

Vorraussetzungen: Das Programm benötigt das .NET Framework 4.6.1

Download: https://bitbucket.org/hwulf/passwordchecker/downloads/passwordchecker_v1.exe 

Kontakt: heiko.wulf@udo.edu


Die API von haveibeenpwned arbeitet, wie dieses Programm auch, unter der CC By 4.0 Lizenz. 
Lizenz: CC By 4.0 (https://creativecommons.org/licenses/by/4.0/deed.de)