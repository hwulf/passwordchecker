﻿namespace passwordchecker
{
    partial class FPassChecker
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.lPassInputText = new System.Windows.Forms.Label();
            this.cbPassVisible = new System.Windows.Forms.CheckBox();
            this.btValidate = new System.Windows.Forms.Button();
            this.lresult = new System.Windows.Forms.Label();
            this.lOneWordResult = new System.Windows.Forms.Label();
            this.linformation = new System.Windows.Forms.Label();
            this.llLicense = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(394, 484);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(226, 17);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "powered by haveibeenpwned.com.";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(145, 44);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(212, 22);
            this.tbPassword.TabIndex = 1;
            this.tbPassword.TextChanged += new System.EventHandler(this.tbPassword_TextChanged);
            this.tbPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPassword_KeyPress);
            // 
            // lPassInputText
            // 
            this.lPassInputText.AutoSize = true;
            this.lPassInputText.Location = new System.Drawing.Point(70, 47);
            this.lPassInputText.Name = "lPassInputText";
            this.lPassInputText.Size = new System.Drawing.Size(69, 17);
            this.lPassInputText.TabIndex = 2;
            this.lPassInputText.Text = "Passwort:";
            // 
            // cbPassVisible
            // 
            this.cbPassVisible.AutoSize = true;
            this.cbPassVisible.Location = new System.Drawing.Point(208, 72);
            this.cbPassVisible.Name = "cbPassVisible";
            this.cbPassVisible.Size = new System.Drawing.Size(149, 21);
            this.cbPassVisible.TabIndex = 3;
            this.cbPassVisible.Text = "Passwort anzeigen";
            this.cbPassVisible.UseVisualStyleBackColor = true;
            this.cbPassVisible.CheckedChanged += new System.EventHandler(this.cbPassVisible_CheckedChanged);
            // 
            // btValidate
            // 
            this.btValidate.Enabled = false;
            this.btValidate.Location = new System.Drawing.Point(363, 44);
            this.btValidate.Name = "btValidate";
            this.btValidate.Size = new System.Drawing.Size(122, 23);
            this.btValidate.TabIndex = 5;
            this.btValidate.Text = "Überprüfen";
            this.btValidate.UseVisualStyleBackColor = true;
            this.btValidate.Click += new System.EventHandler(this.btValidate_Click);
            // 
            // lresult
            // 
            this.lresult.AutoSize = true;
            this.lresult.Location = new System.Drawing.Point(70, 173);
            this.lresult.MaximumSize = new System.Drawing.Size(470, 0);
            this.lresult.Name = "lresult";
            this.lresult.Size = new System.Drawing.Size(468, 17);
            this.lresult.TabIndex = 6;
            this.lresult.Text = "Tippen Sie Ihr Passwort in das Feld ein und drücken Sie auf \"überprüfen\"";
            // 
            // lOneWordResult
            // 
            this.lOneWordResult.AutoSize = true;
            this.lOneWordResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lOneWordResult.Location = new System.Drawing.Point(70, 122);
            this.lOneWordResult.MaximumSize = new System.Drawing.Size(300, 0);
            this.lOneWordResult.Name = "lOneWordResult";
            this.lOneWordResult.Size = new System.Drawing.Size(0, 29);
            this.lOneWordResult.TabIndex = 7;
            // 
            // linformation
            // 
            this.linformation.AutoSize = true;
            this.linformation.Location = new System.Drawing.Point(12, 413);
            this.linformation.MaximumSize = new System.Drawing.Size(480, 0);
            this.linformation.Name = "linformation";
            this.linformation.Size = new System.Drawing.Size(480, 51);
            this.linformation.TabIndex = 8;
            this.linformation.Text = "Hinweis: Die genutzte Datenbank wird von Troy Hunt, dem Betreiber der Website \"ha" +
    "veibeenpwned.com\" bereitgestellt.  Ihr Passwort wird nicht(!) an diese Website ü" +
    "bertragen. Für Details hier klicken.";
            this.linformation.Click += new System.EventHandler(this.linformation_Click);
            // 
            // llLicense
            // 
            this.llLicense.AutoSize = true;
            this.llLicense.Location = new System.Drawing.Point(12, 484);
            this.llLicense.Name = "llLicense";
            this.llLicense.Size = new System.Drawing.Size(121, 17);
            this.llLicense.TabIndex = 9;
            this.llLicense.TabStop = true;
            this.llLicense.Text = "Lizenz: CC BY 4.0";
            this.llLicense.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llLicense_LinkClicked);
            // 
            // FPassChecker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 510);
            this.Controls.Add(this.llLicense);
            this.Controls.Add(this.linformation);
            this.Controls.Add(this.lOneWordResult);
            this.Controls.Add(this.lresult);
            this.Controls.Add(this.btValidate);
            this.Controls.Add(this.cbPassVisible);
            this.Controls.Add(this.lPassInputText);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.linkLabel1);
            this.Name = "FPassChecker";
            this.Text = "Passwort Checker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label lPassInputText;
        private System.Windows.Forms.CheckBox cbPassVisible;
        private System.Windows.Forms.Button btValidate;
        private System.Windows.Forms.Label lresult;
        private System.Windows.Forms.Label lOneWordResult;
        private System.Windows.Forms.Label linformation;
        private System.Windows.Forms.LinkLabel llLicense;
    }
}

