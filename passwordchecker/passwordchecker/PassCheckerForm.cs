﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Security.Cryptography;
using System.IO;

namespace passwordchecker
{
    public partial class FPassChecker : Form
    {
        public FPassChecker()
        {
            InitializeComponent();
        }

        //with best regards to stackoverflow https://stackoverflow.com/questions/17292366/hashing-with-sha1-algorithm-in-c-sharp
        private static string Hashsha1(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach(byte b in hash)
                {
                    sb.Append(b.ToString("X2"));
                }

                return sb.ToString();
            }
        }

        //Link to "haveibeenpwned.com" - They provide the API and the passwort Database
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://haveibeenpwned.com");
        }

        //If checked, the password is displayed in cleartext
        private void cbPassVisible_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPassVisible.Checked)
            {
                tbPassword.PasswordChar = '\0';
            } else
            {
                tbPassword.PasswordChar = '*';
            }
        }

        //if clicked, the programm starts to check if the password is within the haveibeenpwned Database
        private void btValidate_Click(object sender, EventArgs e)
        {
            validate();
        }

        //Method to check if the password is within the haveibeenpwned Database
        private void validate()
        {
            //Create Loading notice
            lOneWordResult.ForeColor = Color.Gray;
            lOneWordResult.Text = "Lädt...";
            lresult.Text = "";
            this.Refresh(); //Refresh Form to display loading notice
            WebClient wc = new WebClient();
            //if not working try:Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705
            wc.Headers.Add("user-agent", "mrbpasswordchecker");

            String sha1pass = Hashsha1(tbPassword.Text.ToString());

            //Generate the API Query String. API requires the first 5 Characters of the sha1 (k-anonymous request)
            StringBuilder sb = new StringBuilder();
            sb.Append("https://api.pwnedpasswords.com/range/");
            sb.Append(sha1pass.Substring(0, Math.Min(sha1pass.Length, 5)));
            String apiquery = sb.ToString();

            //trim sha1pass to match with the suffixes the API returned
            String sha1passtrimmed = sha1pass.Substring(5);

            String result;
            //API will result a list with !suffixex! of the requested first 5 sha1 
            try
            {
                result = wc.DownloadString(apiquery);
            }
            catch (WebException we)
            {
                lOneWordResult.ForeColor = Color.Black;
                lOneWordResult.Text = "Fehler";
                lresult.Text = @"Bei der Verbindung ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Netzwerkverbindung und versuchen Sie es erneut.";
                return;
            }


            using (StringReader reader = new StringReader(result))
            {
                String line;
                bool hit = false;
                while ((line = reader.ReadLine()) != null)
                {
                    //Console.WriteLine(line);
                    //Console.WriteLine(sha1pass);
                    //Check if the complete sha1pass is included in the result of the APIquery
                    if (line.ToUpper().Contains(sha1passtrimmed.ToUpper())) //quick and dirty caseinsensitive check
                    {
                        hit = true;
                        break;
                    }
                }
                if (hit)
                {
                    lOneWordResult.ForeColor = Color.Red;
                    lOneWordResult.Text = "Gefunden!";
                    lresult.Text = @"Ergebnis: Ihr Passwort befindet sich in der Datenbank. 
Das bedeutet, dass dieses Passwort unsicher ist. Sie sollten ein neues Passwort erstellen. 
Prüfen Sie Ihr neues Passwort ebenfalls, bevor Sie es erstellen, um sicherzugehen, dass es sich nicht ebenfalls in der Liste befindet.";
                }
                else
                {
                    lOneWordResult.ForeColor = Color.Green;
                    lOneWordResult.Text = "Nicht gefunden.";
                    lresult.Text = @"Ihr Passwort befindet sich nicht in dieser Datenbank. Dieses Passwort ist vermutlich sicher.
Passwörter sollten dennoch regelmäßig gewechselt werden. Nicht alle jemals gehackten Passwörter befinden sich in der durchsuchten Datenbank.";
                }
            }
        }

        private void tbPassword_TextChanged(object sender, EventArgs e)
        {
            if (tbPassword.Text.Length <= 0)
                btValidate.Enabled = false;
            else
                btValidate.Enabled = true;
        }

        private void linformation_Click(object sender, EventArgs e)
        {
            string infotext =
@"Hinweis: Diese Anwendung verwendet eine Schnittstelle zur Website ""haveibeenpwned.com"". Hierzu wird Ihr eingegebenes Passwort zuerst auf Ihrem Rechner in einen sogenannten Hashwert(SHA1) umgewandelt. Dieser Hashwert ist eine sogenannte Einwegfunktion, das bedeutet, man kann zwar aus dem Passwort einen Hashwert berechnen, nicht jedoch aus dem Hashwert ein Passwort. 
Dieser Hashwert wird darüber hinaus nur zu 1/8 an die Website übertragen. Diese Antwortet mit allen Hashwerten, die mit diesem 1/8 Hashwert beginnen. Dieses Programm prüft dann auf Ihrem Rechner, ob Ihr Passwort in der Antwort vorkam und kann Ihnen daher sagen, ob sich das Passwort in der Datenbank befindet.
Bei diesem Vorgang wird Ihr Passwort daher nicht(!) an dritte Übertragen und verbleibt ausschließlich in der Anwendung auf Ihrem Rechner.
Die Datenbank, die die Passwörter enthält, wird von haveibeenpwned.com bereitgestellt. Diese Seite sammelt in Ihrer Datenbank alle Passwörter, die über die letzten Jahre in diversen Datenlecks vorkamen und die daher als unsicher eingestuft werden müssen. Befindet sich ein Passwort nicht in der Datenbank, heißt dies nicht, dass es nicht trotzdem geknackt wurde.

Lizenz:Creative Commons Attribution 4.0 International (CC BY 4.0)";
            string caption = "Details";

            MessageBox.Show(infotext, caption);
        }

        private void llLicense_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://creativecommons.org/licenses/by/4.0/deed.de");
        }

        private void tbPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                validate();
            }
        }
    }
}
